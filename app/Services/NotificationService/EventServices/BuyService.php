<?php declare(strict_types=1);

namespace App\Services\NotificationService\EventServices;

use App\Entities\NotificationEntity;
use App\Enums\NotificationEventTypeEnum;
use App\Interfaces\EventServiceInterface;
use App\Models\Notification;
use App\Models\Subscription;
use App\Repositories\UserRepository;
use App\Repositories\ProviderRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

class BuyService implements EventServiceInterface
{
    private UserRepository $userRepository;
    private ProviderRepository $providerRepository;

    public function __construct(
        UserRepository $userRepository,
        ProviderRepository $providerRepository
    ) {
        $this->userRepository = $userRepository;
        $this->providerRepository = $providerRepository;
    }

    public function execute(NotificationEntity $notificationEntity): void
    {
        $user = $this->userRepository->getByAppUserId($notificationEntity->getAppUserId());

        DB::beginTransaction();

        try {
            $this->insertNotification($user->id);
            $this->insertSubscription($notificationEntity->getSubscribedUntil(), $user->id);
            $this->insertTransaction($notificationEntity->getProviderType(), $user->id);
        } catch (Throwable $exception) {
            //todo report to logs that something failed
            DB::rollBack();
        }
        DB::commit();
    }

    private function insertTransaction(int $type, $userId): void
    {
        $provider = $this->providerRepository->getProviderFromType($type);

        Subscription::create([
            'user_id' => $userId,
            'provider_id' => $provider->id
        ]);
    }

    private function insertSubscription(Carbon $subscribetUntil, $userId): void
    {
        Subscription::create([
            'user_id' => $userId,
            'subscribed_until' => $subscribetUntil
        ]);
    }

    private function insertNotification(int $userId): void
    {
        Notification::create([
            'user_id' => $userId,
            'event_type' => NotificationEventTypeEnum::INITIAL_BUY
        ]);
    }
}
