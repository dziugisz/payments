<?php declare(strict_types=1);

namespace App\Services\NotificationService\EventServices;

use App\Entities\NotificationEntity;
use App\Interfaces\EventServiceInterface;

class RenewService implements EventServiceInterface
{
    public function execute(NotificationEntity $payload): void
    {

    }
}
