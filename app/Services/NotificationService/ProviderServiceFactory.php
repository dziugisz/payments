<?php declare(strict_types=1);

namespace App\Services\NotificationService;

use App\Enums\NotificationProviderEnum;
use App\Interfaces\ProviderServiceInterface;
use App\Services\NotificationService\ProviderServices\AppleService;
use Illuminate\Support\Facades\App;

class ProviderServiceFactory
{
    public function getProviderService(int $providerEnum): ?ProviderServiceInterface
    {
        switch ($providerEnum) {
            case NotificationProviderEnum::APPLE:
                $response = App::make(AppleService::class);
                break;
//            case NotificationProviderEnum::ANDROID:
//                $response = App::make(AndroidService::class);
//                break;
            default:
                $response = null;
        }

        return $response;
    }
}
