<?php declare(strict_types=1);

namespace App\Services\NotificationService\ProviderServices;

use App\Entities\NotificationEntity;
use App\Enums\NotificationEventTypeEnum;
use App\Enums\NotificationProviderEnum;
use App\Interfaces\ProviderServiceInterface;
use Carbon\Carbon;

class AppleService implements ProviderServiceInterface
{
    public function validate(array $payload): bool
    {
        //todo: validate password, so we would know if its legit request
        return true;
    }

    public function getNotificationEntity(array $payload): NotificationEntity
    {
        $narrowedPayLoad = $payload['responseBody'];

        return (new NotificationEntity())
            ->setEvent($this->extractEvent($narrowedPayLoad))
            ->setProviderType(NotificationProviderEnum::APPLE)
            ->setSubscribedUntil($this->getSubscribedUntil($payload))
            ->setAppUserId($this->getAppUserId($narrowedPayLoad)); // Its a assumption that such field would exist
    }

    private function getSubscribedUntil(array $payload): Carbon
    {
        //todo get legit subscription date from payload
        return Carbon::createFromFormat('Y-m-d H:i:s',  '19/02/2019 00:00:00');
    }

    private function getAppUserId(array $narrowedPayLoad): string
    {
        //todo get legit user id from payload
        return 'example_app_user_id';
    }

    private function extractEvent(array $payload): int
    {
        switch ($payload['notification_type']) {
            case 'INITIAL_BUY':
                $response = NotificationEventTypeEnum::INITIAL_BUY;
                break;
            case 'DID_RENEW':
                $response = NotificationEventTypeEnum::DID_RENEW;
                break;
            case 'DID_FAIL_TO_RENEW':
                $response = NotificationEventTypeEnum::FAIL_TO_RENEW;
                break;
            case 'CANCEL':
                $response = NotificationEventTypeEnum::CANCEL;
                break;
            default:
                $response = null;
        }

        return $response;
    }
}
