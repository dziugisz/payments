<?php declare(strict_types=1);

namespace App\Services\NotificationService;

use App\Entities\NotificationEntity;
use Illuminate\Http\Request;

class NotificationService
{
    private ProviderServiceFactory $providerServiceFactory;
    private EventServiceFactory $eventServiceFactory;

    public function __construct(
        ProviderServiceFactory $providerServiceFactory,
        EventServiceFactory $eventServiceFactory
    ) {
        $this->providerServiceFactory = $providerServiceFactory;
        $this->eventServiceFactory = $eventServiceFactory;
    }

    public function execute(int $providerEnum, array $request): void
    {
        $providerService = $this->providerServiceFactory->getProviderService($providerEnum);
        if ($providerService->validate($request)) {
            $notificationEntity = $providerService->getNotificationEntity($request);
            $this->executeEvent($notificationEntity);
        } else {
            //todo report about invalid json to elastic or something else
        }
    }

    public function executeEvent(NotificationEntity $notificationEntity): void
    {
        $eventService = $this->eventServiceFactory->getEventService($notificationEntity->getEvent());
        $eventService->execute($notificationEntity);
    }
}
