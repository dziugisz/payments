<?php declare(strict_types=1);

namespace App\Services\NotificationService;

use App\Enums\NotificationEventTypeEnum;
use App\Interfaces\EventServiceInterface;
use Illuminate\Support\Facades\App;
use App\Services\NotificationService\EventServices\BuyService;
use App\Services\NotificationService\EventServices\CancelService;
use App\Services\NotificationService\EventServices\FailToRenewService;
use App\Services\NotificationService\EventServices\RenewService;

class EventServiceFactory
{
    public function getEventService(int $eventType): ?EventServiceInterface
    {
        switch ($eventType) {
            case NotificationEventTypeEnum::INITIAL_BUY:
                $response = App::make(BuyService::class);
                break;
            case NotificationEventTypeEnum::DID_RENEW:
                $response = App::make(RenewService::class);
                break;
            case NotificationEventTypeEnum::FAIL_TO_RENEW:
                $response = App::make(FailToRenewService::class);
                break;
            case NotificationEventTypeEnum::CANCEL:
                $response = App::make(CancelService::class);
                break;
            default:
                $response = null;
        }

        return $response;
    }
}
