<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'app_user_id', //I assume that we would have some user ID which would be sent by providers
    ];

    protected $hidden = [
        'password',
    ];
}
