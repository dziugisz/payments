<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Provider;

class ProviderRepository
{
    public function getProviderFromType(int $providerType): ?Provider
    {
        /** @var Provider $provider */
        $provider = Provider::query()->where('type', $providerType)->select('id')->first();

        return $provider;
    }
}
