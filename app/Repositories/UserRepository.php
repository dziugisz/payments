<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function getByAppUserId(string $appUserId): ?User
    {
        /** @var User $user */
        $user = User::query()->where('app_user_id', $appUserId)->select('id')->first();

        return $user;
    }
}
