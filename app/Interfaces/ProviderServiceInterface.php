<?php declare(strict_types=1);

namespace App\Interfaces;

use App\Entities\NotificationEntity;

interface ProviderServiceInterface
{
    public function getNotificationEntity(array $request): NotificationEntity;

    public function validate(array $payload): bool;
}
