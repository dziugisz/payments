<?php declare(strict_types=1);

namespace App\Interfaces;

use App\Entities\NotificationEntity;

interface EventServiceInterface
{
    public function execute(NotificationEntity $payload): void;
}
