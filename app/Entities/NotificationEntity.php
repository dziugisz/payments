<?php declare(strict_types=1);

namespace App\Entities;

use Carbon\Carbon;

class NotificationEntity
{
    private int $event;
    private array $payload;
    private int $providerType;
    private string $appUserId;
    private ?Carbon $subscribedUntil = null;

    public function getSubscribedUntil(): ?Carbon
    {
        return $this->subscribedUntil;
    }

    public function setSubscribedUntil(Carbon $subscribedUntil): NotificationEntity
    {
        $this->subscribedUntil = $subscribedUntil;

        return $this;
    }

    public function getAppUserId(): string
    {
        return $this->appUserId;
    }

    public function setAppUserId(string $appUserId): NotificationEntity
    {
        $this->appUserId = $appUserId;

        return $this;
    }

    public function getProviderType(): int
    {
        return $this->providerType;
    }

    public function setProviderType(int $providerType): NotificationEntity
    {
        $this->providerType = $providerType;

        return $this;
    }

    public function getEvent(): int
    {
        return $this->event;
    }

    public function setEvent(int $event): NotificationEntity
    {
        $this->event = $event;

        return $this;
    }
}
