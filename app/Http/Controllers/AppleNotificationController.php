<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\NotificationProviderEnum;
use App\Services\NotificationService\NotificationService;
use Illuminate\Http\Request;

class AppleNotificationController
{
    public function __invoke(Request $request, NotificationService $notificationService): void
    {
        $notificationService->execute(NotificationProviderEnum::APPLE, json_decode($request->json(), true));
    }
}
