<?php declare(strict_types=1);

namespace App\Enums;

class NotificationProviderEnum
{
    public const APPLE = 1;
    public const ANDROID = 2;
}
