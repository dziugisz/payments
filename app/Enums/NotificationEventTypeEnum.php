<?php declare(strict_types=1);

namespace App\Enums;

class NotificationEventTypeEnum
{
    public const INITIAL_BUY = 1;
    public const DID_RENEW = 2;
    public const FAIL_TO_RENEW = 3;
    public const CANCEL = 4;
}
