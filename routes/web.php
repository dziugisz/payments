<?php declare(strict_types=1);

use App\Http\Controllers\AppleNotificationController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('apple-notification-webhook', AppleNotificationController::class);
//Route::get('android-notification-webhook', AndroidNotificationController::class);
